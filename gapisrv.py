#!/usr/bin/env python

# This program acts as a simple webserver and tries to find whatever
# is in the path of the request locally or from ajax.googleapis.com

# TODO
# - Actually attempt to validate the url
# - Listen on port 443? SSL?
# - The 'latest' version directory could get out of date so fix that
# - Learn2iptable so we don't have to run this as root
# - How to figure out REQUEST_URI? What if it isn't ajax.googleapis.com?
# - urllib doesn't validate certs... should probably do something about that

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from urllib import urlretrieve
import os

# These two determine who we ask for a file if it doesn't exist locally
protocol = 'https'
site_name = 'ajax.googleapis.com'

# The port to listen on (ports lower than 1024 need root)
port = 80

class GoogleAPIHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        local_path = '{}{}'.format(site_name, self.path)
        # Download file if it doesn't exist
        if not os.path.isfile(local_path):
            # Create directory if it does not exist
            local_dir = os.path.dirname(local_path)
            if not os.path.isdir(local_dir):
                os.makedirs(local_dir)
            # Download library from Google API
            urlretrieve('{}://{}{}'.format(protocol, site_name, self.path),
                        local_path)
        # Open file and send it
        message = open(local_path, 'r')
        
        self.send_response(200)
        self.end_headers()
        self.wfile.write(message.read())
	message.close()
        return

if __name__ == '__main__':
	server = HTTPServer(('', port), GoogleAPIHandler)
	print 'Starting server, use <Ctrl-C> to stop'
	# This does exception handling for us so don't bother with that shit
	server.serve_forever()

